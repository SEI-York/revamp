from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.forms.widgets import PasswordInput, TextInput
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset,  ButtonHolder, Submit

from contact_form.forms import ContactForm


class CustomAuthForm(AuthenticationForm):
    username = forms.CharField(widget=TextInput(
        attrs={'class': 'validate', 'placeholder': 'Email'}))
    password = forms.CharField(widget=PasswordInput(
        attrs={'placeholder': 'Password'}))


class CustomContactForm(ContactForm):
    CHOICES = [
        ('QUESTION', 'Question'),
        ('COMMENT', 'Comment'),
        ('BUG', 'Bug report'),
        ('FEATURE', 'Feature request'),
    ]
    topic = forms.ChoiceField(choices=CHOICES)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'topic',
            'name',
            'email',
            'body',
            ButtonHolder(
                Submit('submit', 'Submit', css_class='btn btn-success btn-block')
            )
        )
